# import required modules
import socket
import threading

# Host has to be a string in form of IPV4 address
# Port can be any number between 0 to 65535
HOST = "127.0.0.1"
PORT = 1235
LISTENER_LIMIT = 5
active_clients = []


# Function to listen for new messages from a client
def listen_for_messages(client, username):
    while 1:
        message = client.recv(2048).decode("utf-8")

        if message != "":
            final_msg = username + "~" + message
            send_messages_to_all(final_msg)
        else:
            print(f"the message fom client {username} is empty")


# function to send message to as single client
def send_message_to_client(client, message):
    print(message)
    client.sendall(message.encode())


# Function to send any new message to all the clients that are currently connected to the server
def send_messages_to_all(message):
    for user in active_clients:
        send_message_to_client(user[1], message)


# Function to handle client
def client_handler(client):
    # Server will listen for client message that contains username
    while 1:
        username = client.recv(2048).decode("utf-8")
        if username != "":
            active_clients.append((username, client))
            prompt_mesage = f"SERVER~{username} has been added to the chat "
            send_messages_to_all(prompt_mesage)
            break

        else:
            print("Client username is empty/invalid")

    threading.Thread(
        target=listen_for_messages,
        args=(
            client,
            username,
        ),
    ).start()


# Main function
def main():
    # Creating the socket class object
    # IPV4 v IPV6 ; AF_INET specifies IPV4
    # SOCK_STREAM specifies TCP packets (rather than UDP which would be SOCK_DGRAM)
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # bind server to the host and the port
    try:
        # Provid
        server.bind((HOST, PORT))
        print(f"Running the server on  {HOST} {PORT}")
    except Exception as e:
        print(f"Unable to bind to {HOST} and port {PORT}")
        pass
    # Set server limit
    server.listen(LISTENER_LIMIT)

    # this while loop will keep listening to client connections:
    while 1:
        client, address = server.accept()
        # addresss is a tuple (CLIENT_HOST, CLIENT_PORT)
        print(f"Succesfully connected to client {address[0]} {address[1]}")

        threading.Thread(target=client_handler, args=(client,)).start()


if __name__ == "__main__":
    main()
